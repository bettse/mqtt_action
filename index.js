const os = require('os')
const mqtt = require('mqtt')
const client = mqtt.connect('mqtt://hassbian.local', {username: 'nespinfc', password: 'nespinfc'})
const { exec } = require('child_process');

client.on('connect', function () {
  console.log('MQTT client connected')
  client.subscribe('tag')
  client.on('message', function(topic, message) {
    console.log('new message')
    const tag = JSON.parse(message)
    if (tag.ndef) {
      openUri(tag.ndef)
    }
  })
})

function openUri(records) {
  records.forEach((record) => {
    if (record.recordType == 'RTD_URI') {
      console.log(`opening ${record.value}`)
      open(record.value)
    } else if (record.recordType == 'RTD_SMART_POSTER') {
      openUri(record.value)
    }
  })
}

function execCallback(err, stdout, stderr) {
  if (err) {
    // node couldn't execute the command
    console.log('Error executing:', err)
    return
  }

  // the *entire* stdout and stderr (buffered)
  if (stdout.length > 0) {
    console.log(`stdout: ${stdout}`)
  }
  if (stderr.length > 0) {
    console.log(`stderr: ${stderr}`)
  }
}

function open(value) {
  switch(os.type()) {
  case 'Darwin':
    exec(`open ${value}`, execCallback)
    break
  case 'Linux':
    exec('xscreensaver-command -deactivate', () => {
      exec(`DISPLAY=:0 chromium-browser -start-maximized ${value}`, execCallback)
    })
    break
  default:
    console.log('Unsupported OS:', os.type())
    break
  }
}
